package genesis.alava.convertidordelibragenesisalava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ResultadoActivity extends AppCompatActivity {

    TextView lblResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        Bundle bundle = this.getIntent().getExtras();
        String resultado = bundle.getString("resultado");

        lblResultado = (TextView) findViewById(R.id.lblResultado);

        lblResultado.setText("El resultado es: " + resultado + "kilogramos");

        Log.e("conversion","Conversion Terminada");
    }
}
