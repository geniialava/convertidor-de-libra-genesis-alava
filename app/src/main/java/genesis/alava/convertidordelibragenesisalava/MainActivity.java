package genesis.alava.convertidordelibragenesisalava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText txtNumero;
    Button btnConvertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNumero = (EditText) findViewById(R.id.txtNumero);
        btnConvertir = (Button) findViewById(R.id.btnConvertir);

        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), ResultadoActivity.class);
                float i = Float.parseFloat(txtNumero.getText().toString());
                float valorkilogramos =  Float.parseFloat("0.453592");
                float resultadoFloat = i*valorkilogramos;
                String resultadoString = Float.toString(resultadoFloat);
                Bundle bundle = new Bundle();
                bundle.putString("resultado", resultadoString);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
